﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public GameObject pauseMenu;
	public GameObject gameOverMenu;
	public GameObject levelCompleteMenu;

	public GameObject damageEnemy;
	public GameObject damagePlayer;

	public GameObject darkParticles;

	public int level;
	public bool isGameInPause;

	public bool isPlayerTurn;

	public bool isAttackFinished;

	private bool isAppearDone;

	public Transform firePrefab;
	public Transform icePrefab;
	public Transform thunderPrefab;
	public Transform lightPrefab;
	public GameObject flashLight;

	private GameObject[] spawnEnemies;
	private GameObject player;
	private PlayerInfo playerInfo;

	private Zone zone;
	private GameObject map;

	private float timeWaitDamage = 2f;
	private float timeWaitSpriteDamage = 0.5f;

	// Use this for initialization
	void Start () {
		
		SaveLoadGame.Load ();

		playerInfo = SaveLoadGame.playerSave;

		spawnEnemies = GameObject.FindGameObjectsWithTag("SpawnEnemy");

		player = GameObject.FindGameObjectWithTag ("Player");
		player.transform.localScale = new Vector3 (5, 5, 1);
		player.GetComponent<Animator> ().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController> ("Animation/Characters/Character1Animator");
		player.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Sprites/Characters/character1");

		playerInfo.healAll ();

		if (SaveLoadGame.idZone == -1) {
			zone = Zone.getZoneById (1);
		} else {
			zone = Zone.getZoneById (SaveLoadGame.idZone);
		}

		level = zone.minLevel;

		GameObject displayMap = Instantiate (Resources.Load<GameObject> (zone.map));
		displayMap.transform.position = new Vector3(-15.0f, 21.8f, 0.0f);
		displayMap.transform.localScale = new Vector3(0.005f, 0.005f, 1.0f);
		map = displayMap;

		GameObject moneyObject = GameObject.Find ("MoneyText");
		moneyObject.GetComponent<Text> ().text = "" + playerInfo.money;

		//pour charger le premier niveau
		setSceneLevel ();
		isPlayerTurn = true;
	}

	/**
	 * Affiche le menu pause du jeu
	 */
	public void showPauseMenu(){
		pauseMenu.SetActive (true);
		isGameInPause = true;
		disableAttackButtons ();
	}

	/**
	 * Cache le menu de pause du jeu
	 */
	public void hidePauseMenu(){
		pauseMenu.SetActive (false);
		isGameInPause = false;
		enableAttackButtons ();
	}

	/**
	 * Permet de désactiver les boutons d'attaques du joueur
	 */
	public void disableAttackButtons(){

		GameObject attackButton = GameObject.Find("AttackImage");
		GameObject fireButton = GameObject.Find ("FireImage");
		GameObject iceButton = GameObject.Find("IceImage");
		GameObject thunderButton = GameObject.Find ("ThunderImage");
		GameObject lightButton = GameObject.Find ("LightImage");

		attackButton.GetComponent<Button> ().interactable = false;
		fireButton.GetComponent<Button> ().interactable = false;
		iceButton.GetComponent<Button> ().interactable = false;
		thunderButton.GetComponent<Button> ().interactable = false;
		lightButton.GetComponent<Button> ().interactable = false;

	}

	/**
	 * Permet d'activer les boutons d'attaques du joueur
	 */
	public void enableAttackButtons(){

		GameObject attackButton = GameObject.Find("AttackImage");
		GameObject fireButton = GameObject.Find ("FireImage");
		GameObject iceButton = GameObject.Find("IceImage");
		GameObject thunderButton = GameObject.Find ("ThunderImage");
		GameObject lightButton = GameObject.Find ("LightImage");

		attackButton.GetComponent<Button> ().interactable = true;
		fireButton.GetComponent<Button> ().interactable = true;
		iceButton.GetComponent<Button> ().interactable = true;
		thunderButton.GetComponent<Button> ().interactable = true;
		lightButton.GetComponent<Button> ().interactable = true;

	}

	/**
	 * Permet de retourner au menu principal du jeu
	 */
	public void returnToMainMenu(){
		playerInfo.setMaxLevelReach(level);
		SaveLoadGame.Save ();
		SceneManager.LoadScene ("menu");
	}

	/**
	 * Indique que l'on continue de jouer
	 */
	public void continueGame(){
		levelCompleteMenu.SetActive (false);
		changeLevel ();
	}

	/**
	 * Permet de relancer le jeu
	 */
	public void retryGame(){
		playerInfo.setMaxLevelReach(level);
		SaveLoadGame.Save ();
		SceneManager.LoadScene ("overworld");
	}

	/**
	 * Permet de lancer une attaque basique
	 */
	public void playerAttack(){ //TODO
		
		StartCoroutine (launchAttack ("sword"));
		StartCoroutine (swordAttack ());

	}

	/**
	 * Permet de lancer le sort de feu
	 */
	public void playerUseFire(){

		if (playerInfo.mana >= 1) {
			Instantiate (firePrefab, player.transform.position, Quaternion.Euler (0.0f, 0.0f, 217.5f));
			StartCoroutine (launchAttack ("fire"));
		}

	}

	/**
	 * Permet de lancer le sort de glace
	 */
	public void playerUseIce(){

		if (playerInfo.mana >= 1) {
			Instantiate (icePrefab, player.transform.position, Quaternion.Euler(0.0f, 0.0f, 140f));
			StartCoroutine (launchAttack ("ice"));
		}

	}

	/**
	 * Permet de lancer le sort de foudre
	 */
	public void playerUseThunder(){ //TODO

		if (playerInfo.mana >= 4) {
			Instantiate (thunderPrefab, player.transform.position, Quaternion.identity);
			StartCoroutine (launchAttack ("thunder"));
		}

	}

	/**
	 * Permet de lancer le sort de lumière
	 */
	public void playerUseLight(){ //TODO

		if (playerInfo.mana >= 4) {
			Instantiate (lightPrefab, player.transform.position, Quaternion.identity);
			StartCoroutine (launchAttack ("light"));
		}

	}

	/**
	 * Initialise le niveau
	 */
	public void setSceneLevel(){

		GameObject levelNameObject = GameObject.Find ("LevelName");
		levelNameObject.GetComponent<Text> ().text = "Level : " + level;

		int nbEnemies = Random.Range (1, 4);

		GameObject[] allEnemies = new GameObject[nbEnemies];

		for (int i = 1; i <= nbEnemies; i++) {

			GameObject spawn = spawnEnemies [i-1];

			//sprite ennemie à récupérer et à mettre
			GameObject enemy = Instantiate(Resources.Load<GameObject> ("Prefabs/Enemy"));
			enemy.transform.position = spawn.transform.position;
			enemy.transform.localScale = new Vector3 (5, 5, 1);

			enemy.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Sprites/Enemies/"+zone.enemySprite);
			enemy.GetComponent<Animator> ().runtimeAnimatorController = Resources.Load<RuntimeAnimatorController> ("Animation/Enemies/"+zone.enemyAnimator);

			enemy.GetComponent<EnemyEntity> ().initEnemy (zone.enemyClassName);

			allEnemies [i - 1] = enemy;

		}

		StartCoroutine (enemyAppear(allEnemies));

		if (zone.isDarkParticleFall) {
			darkParticles.SetActive (true);
		} else {
			darkParticles.SetActive (false);
		}

	}

	/**
	 * Retourne le nombre d'ennemis qui sont encore en vie
	 */
	public int getNbEnemyAlive(){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		int nb = 0;

		for (int i = 0; i < enemies.Length; i++) {

			GameObject enemy = enemies [i];
			Enemy info = enemy.GetComponent<EnemyEntity> ().info;
			if (!info.getIsDead ()) {
				nb++;
			}

		}

		return nb;
	}

	/**
	 * Met à jour les informations sur les ennemis
	 */
	public void verifyEnemiesStatus(){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		if (enemies.Length > 0) {
			bool deadEnemy = false;

			for (int i = 0; i < enemies.Length; i++) {

				GameObject enemy = enemies [i];
				Enemy info = enemy.GetComponent<EnemyEntity> ().info;
				if (info.getIsDead ()) {
					StartCoroutine(enemyDisappear(enemy));
					deadEnemy = true;
				}
			}

			if (deadEnemy) {
				GameObject moneyObject = GameObject.Find ("MoneyText");
				moneyObject.GetComponent<Text> ().text = "" + playerInfo.money;
			}

		}

	}

	/**
	 * Change le niveau
	 */
	public void changeLevel(){

		playerInfo.heal ((playerInfo.getLevelLifeRegen () + 1) * 5);
		playerInfo.healMana ((playerInfo.getLevelManaRegen () + 1) * 5);

		updateLifeBar ();
		updateMana ();

		level++;

		if (level > 100) {

			if (map != null)
				Destroy (map);
			else
				Destroy (GameObject.Find (zone.map));
			
			SceneManager.LoadScene ("credits");

		} else {
			if (zone.maxLevel < level) {

				Zone previous = zone;
				if (map == null)
					Destroy (GameObject.Find (previous.map));
				else
					Destroy (map);
			
				zone = Zone.getZoneById (zone.id + 1);

				GameObject displayMap = Instantiate (Resources.Load<GameObject> (zone.map));
				displayMap.transform.position = new Vector3 (-15.0f, 21.8f, 0.0f);
				displayMap.transform.localScale = new Vector3 (0.005f, 0.005f, 1.0f);
				map = displayMap;

				SaveLoadGame.idZone = zone.id;
			}

			setSceneLevel ();

		}

	}

	/**
	 * Permet d'infliger des degats aux ennemis
	 */
	public void setEnemiesDamage(int damage){

		StartCoroutine (damageAppearEnemy (damage));

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy"); 

		for(int i = 0; i < enemies.Length; i++){

			GameObject enemy = enemies [i];
			Enemy info = enemy.GetComponent<EnemyEntity> ().info;
			info.takeDamage (damage);
		
		}

		if (getNbEnemyAlive () > 0) {
			
			verifyEnemiesStatus ();
			//endTurnPlayer ();
			enemyAttack ();
			if(!playerInfo.getIsDead()){
				startTurnPlayer ();
			} else {
				gameOverMenu.SetActive (true);
			}

		} else {
			verifyEnemiesStatus ();
			levelCompleteMenu.SetActive (true);
			disableAttackButtons ();
		}

	}

	/**
	 * Met à jour la barre de vie
	 */
	public void updateLifeBar(){

		float max = playerInfo.lifeMax;
		float current = playerInfo.life;

		float taux = (float)current / (float)max;

		GameObject manaBar = GameObject.Find ("LifeBarFiller");
		RectTransform rect = manaBar.GetComponent<RectTransform> ();
		rect.sizeDelta = new Vector2 (taux * 100, 10);

	}

	/**
	 * Met à jour la barre de mana ainsi que les boutons pouvant etre utilise 
	 */
	public void updateMana(){

		float max = playerInfo.manaMax;
		float current = playerInfo.mana;

		float taux = (float)current / (float)max;

		GameObject manaBar = GameObject.Find ("ManaBarFiller");
		RectTransform rect = manaBar.GetComponent<RectTransform> ();
		rect.sizeDelta = new Vector2 (taux * 100, 10);

		GameObject fireButton = GameObject.Find ("FireImage");
		GameObject iceButton = GameObject.Find ("IceImage");
		GameObject thunderButton = GameObject.Find ("ThunderImage");
		GameObject lightButton = GameObject.Find ("LightImage");

		if (playerInfo.mana < 1) {

			fireButton.GetComponent<Button> ().interactable = false;
			iceButton.GetComponent<Button> ().interactable = false;

		} else {
			fireButton.GetComponent<Button> ().interactable = true;
			iceButton.GetComponent<Button> ().interactable = true;
		}

		if (playerInfo.mana < 4) {
			thunderButton.GetComponent<Button> ().interactable = false;
			lightButton.GetComponent<Button> ().interactable = false;
		} else {
			thunderButton.GetComponent<Button> ().interactable = true;
			lightButton.GetComponent<Button> ().interactable = true;
		}


	}

	/**
	 * Permet aux ennemis d'attaquer
	 */
	public void enemyAttack(){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		int totalDamage = 0;

		for (int i = 0; i < enemies.Length; i++) {

			GameObject enemy = enemies [i];
			Enemy info = enemy.GetComponent<EnemyEntity> ().info;
			playerInfo.TakeDamage (info.damage);
			totalDamage += info.damage;

		}

		updateLifeBar();

		StartCoroutine (enemyAnimationAttack ());

		StartCoroutine (damageAppearPlayer (totalDamage));

	}

	/**
	 * Met fin au tour du joueur
	 */
	public void endTurnPlayer(){

		isPlayerTurn = false;

		disableAttackButtons ();

	}

	/**
	 * Lancer le tour du joueur
	 */
	public void startTurnPlayer(){

		isPlayerTurn = true;

		enableAttackButtons ();

	}

	/**
	 * Est appele pour gerer les differents affichage lorsque les ennemis prennent des degats
	 */
	IEnumerator damageAppearEnemy(int damage){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		for (int i = 0; i < enemies.Length; i++) {
			if(enemies[i] != null)
				enemies [i].GetComponent<SpriteRenderer> ().color = Color.red;
		}

		damageEnemy.GetComponent<Text> ().text = "-" + damage;
		yield return new WaitForSeconds (timeWaitSpriteDamage);

		for (int i = 0; i < enemies.Length; i++) {
			if(enemies[i] != null)
				enemies [i].GetComponent<SpriteRenderer> ().color = Color.white;
		}

		yield return new WaitForSeconds (timeWaitDamage - timeWaitSpriteDamage);
		damageEnemy.GetComponent<Text> ().text = "";

	}

	/**
	 * Est appele pour gerer les differents affichages lorsque le joueur prend des degats
	 */
	IEnumerator damageAppearPlayer(int damage){

		Camera camera = Camera.main;

		float deplacement = 0.5f;

		float x = camera.transform.position.x + deplacement;
		float y = camera.transform.position.y;
		float z = camera.transform.position.z;

		camera.transform.position = new Vector3(x, y, z);
		x-=deplacement;
		yield return new WaitForEndOfFrame();
		camera.transform.position = new Vector3(x, y, z);
		x-=deplacement;
		yield return new WaitForEndOfFrame();
		camera.transform.position = new Vector3(x, y, z);
		x+=deplacement;
		yield return new WaitForEndOfFrame();
		camera.transform.position = new Vector3(x, y, z);

		player.GetComponent<SpriteRenderer> ().color = Color.red;
		damagePlayer.GetComponent<Text> ().text = "-" + damage;

		yield return new WaitForSeconds (timeWaitSpriteDamage);

		player.GetComponent<SpriteRenderer> ().color = Color.white;

		yield return new WaitForSeconds (timeWaitDamage - timeWaitSpriteDamage);

		damagePlayer.GetComponent<Text> ().text = "";
	}

	/**
	 * Est appele pour gerer les differents affichages lorsqu'une attaque est lance
	 */
	IEnumerator launchAttack(string attackName){

		isAttackFinished = false;

		endTurnPlayer ();

		yield return new WaitWhile(() => !isAttackFinished);

		int damage = 2;
		int manaUsed = 1;

		switch (attackName) {
		case "sword":
			damage = 1 * (playerInfo.getLevelAttack ()+1);
			manaUsed = 0;

			float r = Random.Range (0.0f, 1.0f);
			if (r < playerInfo.getLevelCritic () * 0.01f)
				damage *= 2;

			break;
		case "fire":

			damage = 2 * (playerInfo.getLevelFire ()+1);

			break;
		case "ice":

			damage = 2 * (playerInfo.getLevelIce () + 1);

			break;
		case "thunder":
			damage = 4 * (playerInfo.getLevelThunder() + 1);
			manaUsed = 3;
			break;
		case "light":
			damage = 4 * (playerInfo.getLevelLight() + 1);
			manaUsed = 3;
			break;
		}

		setEnemiesDamage (damage);
		playerInfo.useMana (manaUsed);

		updateMana ();

		yield return null;
	}

	/**
	 * Est appelee pour faire un fondu d'apparition d'un groupe d'ennemis
	 * Desactive les boutons d'actions et les reactive une fois les ennemis apparus
	 */
	IEnumerator enemyAppear(GameObject[] allEnemies){

		disableAttackButtons ();

		for (float i = 0.0f; i <= 1; i+=0.1f) {

			for (int e = 0; e < allEnemies.Length; e++) {

				Color c = allEnemies[e].GetComponent<SpriteRenderer> ().color;
				c.a = i;
				allEnemies[e].GetComponent<SpriteRenderer> ().color = c;

			}
			yield return new WaitForSeconds (0.1f);
		}

		enableAttackButtons ();

		yield return null;
	}

	/**
	 * Est appelee pour faire un fondu de disparition d'un ennemi
	 */
	IEnumerator enemyDisappear(GameObject enemyDisappeared){

		for (float i = 0.0f; i <= 1; i+=0.1f) {

			Color c = enemyDisappeared.GetComponent<SpriteRenderer> ().color;
			c.a = 1.0f - i;
			enemyDisappeared.GetComponent<SpriteRenderer> ().color = c;
				
			yield return new WaitForSeconds (0.1f);
		}

		enemyDisappeared.GetComponent<EnemyEntity>().death ();

		yield return null;
	}

	IEnumerator swordAttack(){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		player.transform.position = new Vector3(0.0f, 2.0f, 0.0f);

		for (int i = 0; i < enemies.Length; i++) {
			GameObject spawn = spawnEnemies [i];
			spawn.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite>("Sprites/Spells/damage");
		}

		yield return new WaitForSeconds (0.1f);

		player.transform.position = new Vector3(0.0f, 0.0f, 0.0f);

		for (int i = 0; i < enemies.Length; i++) {
			GameObject spawn = spawnEnemies [i];
			spawn.GetComponent<SpriteRenderer> ().sprite = null;
		}

		isAttackFinished = true;

		yield return null;
	}

	IEnumerator enemyAnimationAttack(){

		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		for (int i = 0; i < enemies.Length; i++) {
			Vector3 pos = enemies [i].transform.position;
			pos.y -= 2;
			enemies [i].transform.position = pos;
		}

		yield return new WaitForSeconds (0.1f);

		for (int i = 0; i < enemies.Length; i++) {
			Vector3 pos = enemies [i].transform.position;
			pos.y += 2;
			enemies [i].transform.position = pos;
		}

		isAttackFinished = true;

		yield return null;
	}
}
