﻿using UnityEngine;
using System.Collections;

public class Zone {

	public int id;

	public int minLevel;
	public int maxLevel;

	public int idZoneNext; //set at -1 for the end
	public string map;

	public string enemySprite;
	public string enemyAnimator;
	public string enemyClassName;

	public bool isDarkParticleFall;

	public Zone(){

	}

	public static Zone getZoneById(int identifiant){

		Zone z = new Zone ();

		z.isDarkParticleFall = false;

		switch (identifiant) {

		case 1: //forest
			z.id = 1;
			z.minLevel = 1;
			z.maxLevel = 10;

			z.idZoneNext = 2;
			z.map = "forest";

			z.enemySprite = "GreenSlime";
			z.enemyAnimator = "GreenSlimeAnimator";
			z.enemyClassName = EnemyFactory.GREEN_SLIME;
			break;

		case 2: //plain
			z.id = 2;
			z.minLevel = 11;
			z.maxLevel = 20;

			z.idZoneNext = 3;
			z.map = "plain";

			z.enemySprite = "BlueSlime";
			z.enemyAnimator = "BlueSlimeAnimator";
			z.enemyClassName = EnemyFactory.BLUE_SLIME;
			break;

		case 3: //carriere
			z.id = 3;
			z.minLevel = 21;
			z.maxLevel = 30;

			z.idZoneNext = 4;
			z.map = "carriere";

			z.enemySprite = "RedSlime";
			z.enemyAnimator = "RedSlimeAnimator";
			z.enemyClassName = EnemyFactory.RED_SLIME;
			break;

		case 4: //cavern
			z.id = 4;
			z.minLevel = 31;
			z.maxLevel = 40;

			z.idZoneNext = 5;
			z.map = "cavern";

			z.enemySprite = "Bat";
			z.enemyAnimator = "BatAnimator";
			z.enemyClassName = EnemyFactory.BAT;
			break;

		case 5: //champ
			z.id = 5;
			z.minLevel = 41;
			z.maxLevel = 50;

			z.idZoneNext = 6;
			z.map = "champ";

			z.enemySprite = "Pumpking";
			z.enemyAnimator = "PumpkingAnimator";
			z.enemyClassName = EnemyFactory.PUMPKING;
			break;

		case 6: //dark forest

			z.id = 6;
			z.minLevel = 51;
			z.maxLevel = 60;

			z.idZoneNext = 7;
			z.map = "darkforest";

			z.enemySprite = "Worm";
			z.enemyAnimator = "WormAnimator";
			z.enemyClassName = EnemyFactory.WORM;

			z.isDarkParticleFall = true;
			break;
		case 7: //cimetiere

			z.id = 7;
			z.minLevel = 61;
			z.maxLevel = 70;

			z.idZoneNext = 8;
			z.map = "cimetiere";

			z.enemySprite = "Ghost";
			z.enemyAnimator = "GhostAnimator";
			z.enemyClassName = EnemyFactory.GHOST;

			z.isDarkParticleFall = true;
			break;

		case 8: //city

			z.id = 8;
			z.minLevel = 71;
			z.maxLevel = 80;

			z.idZoneNext = 9;
			z.map = "city";

			z.enemySprite = "Deadman";
			z.enemyAnimator = "DeadmanAnimator";
			z.enemyClassName = EnemyFactory.DEADMAN;

			z.isDarkParticleFall = true;
			break;

		case 9: //palace

			z.id = 9;
			z.minLevel = 81;
			z.maxLevel = 90;

			z.idZoneNext = 10;
			z.map = "palace";

			z.enemySprite = "DeadSoldier";
			z.enemyAnimator = "DeadSoliderAnimator";
			z.enemyClassName = EnemyFactory.DEADSOLDIER;

			z.isDarkParticleFall = true;
			break;

		case 10: //darkness

			z.id = 10;
			z.minLevel = 91;
			z.maxLevel = 100;

			z.idZoneNext = -1;
			z.map = "darkness";

			z.enemySprite = "Eyeball";
			z.enemyAnimator = "EyeballAnimator";
			z.enemyClassName = EnemyFactory.EYEBALL;

			z.isDarkParticleFall = true;
			break;
		}

		return z;
	}
}
