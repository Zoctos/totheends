﻿using UnityEngine;
using System.Collections;

public class Enemy {

	public int life;
	public int lifeMax;

	public int damage;
	public int money;

	public bool isDead;

	public Enemy(){

		this.isDead = false;

	}

	public int getLife(){
		return this.life;
	}

	public int getLifeMax(){
		return this.lifeMax;
	}

	public bool getIsDead(){
		return isDead;
	}

	public void takeDamage(int playerDamage){

		if (life - playerDamage <= 0) {

			damage = 0;
			isDead = true;

		} else {
			life -= playerDamage;
		}

	}

	public void healAll(){

		this.isDead = false;
		life = lifeMax;

	}

}
