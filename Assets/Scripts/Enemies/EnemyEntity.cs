﻿using UnityEngine;
using System.Collections;

public class EnemyEntity : MonoBehaviour {

	public Enemy info;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
	
	}

	public void initEnemy(string name){

		info = EnemyFactory.createEnemy (name);

		info.healAll ();

	}

	public void death(){

		if (info.getIsDead ()) {

			PlayerInfo player = SaveLoadGame.playerSave;
			player.addMoney (info.money);

			Destroy (gameObject);

		}

	}
		
}
