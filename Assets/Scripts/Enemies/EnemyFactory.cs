﻿using UnityEngine;
using System.Collections;

public class EnemyFactory {

	public const string GREEN_SLIME = "GreenSlime";
	public const string BLUE_SLIME = "BlueSlime";
	public const string RED_SLIME = "RedSlime";
	public const string BAT = "Bat";
	public const string PUMPKING = "Pumpking";
	public const string WORM = "Worm";
	public const string GHOST = "Ghost";
	public const string DEADMAN = "Deadman";
	public const string DEADSOLDIER = "Deadsoldier";
	public const string EYEBALL = "Eyeball";

	public static Enemy createEnemy(string name){

		switch (name) {

		case GREEN_SLIME:
			return new GreenSlime ();
			break;

		case BLUE_SLIME:
			return new BlueSlime ();
			break;

		case RED_SLIME:
			return new RedSlime ();
			break;

		case BAT:
			return new Bat ();
			break;

		case PUMPKING:
			return new Pumpking ();
			break;

		case WORM:
			return new Worm ();
			break;

		case GHOST:
			return new Ghost ();
			break;

		case DEADMAN:
			return new DeadMan();
			break;

		case DEADSOLDIER:
			return new DeadSoldier ();
			break;

		case EYEBALL:
			return new Eyeball ();
			break;

		}

		return null;

	}

}

public class GreenSlime : Enemy {

	public GreenSlime() : base(){

		lifeMax = 10;
		damage = 1;
		money = Random.Range (1, 5);

	}

}

public class BlueSlime : Enemy{

	public BlueSlime() : base(){

		lifeMax = 20;
		damage = 2;
		money = Random.Range (5, 10);

	}

}

public class RedSlime : Enemy{

	public RedSlime() : base(){

		lifeMax = 30;
		damage = 3;
		money = Random.Range (10, 15);

	}

}

public class Bat : Enemy{

	public Bat() : base(){

		lifeMax = 10;
		damage = 6;
		money = Random.Range (15, 20);

	}

}

public class Pumpking : Enemy{

	public Pumpking() : base(){

		lifeMax = 60;
		damage = 1;
		money = Random.Range (20, 25);

	}

}

public class Worm : Enemy{

	public Worm() : base(){

		lifeMax = 60;
		damage = 6;
		money = Random.Range(25, 30);

	}

}

public class Ghost : Enemy{

	public Ghost() : base(){

		lifeMax = 50;
		damage = 5;
		money = Random.Range (30, 35);

	}

}

public class DeadMan : Enemy{

	public DeadMan() : base(){

		lifeMax = 80;
		damage = 10;
		money = Random.Range (0, 35);

	}

}

public class DeadSoldier : Enemy{

	public DeadSoldier() : base(){

		lifeMax = 100;
		damage = 10;
		money = Random.Range (0, 40);

	}

}

public class Eyeball : Enemy{

	public Eyeball() : base(){

		lifeMax = 200;
		damage = 20;
		money = Random.Range (0, 50);

	}

}