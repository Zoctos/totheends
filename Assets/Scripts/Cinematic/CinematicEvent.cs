﻿using UnityEngine;
using System.Collections;

public class CinematicEvent : MonoBehaviour{

	private int beginStep;
	private int endStep;
	private bool isActif;

	public CinematicEvent(int beginStep, int endStep){
		this.beginStep = beginStep;
		this.endStep = endStep;
		this.isActif = false;
	}

	public void startEvent(){
		isActif = true;
	}

	public void endEvent(){
		Destroy (this);	
	}
		
	public void setIsActif(bool isActif){
		this.isActif = isActif;
	}

	public int getBeginStep(){
		return this.beginStep;
	}
	public int getEndStep(){
		return this.endStep;
	}

}