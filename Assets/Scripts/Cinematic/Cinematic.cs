﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Cinematic{

	private int numberStep;
	private List<CinematicEvent> events;
	private Dictionary<int, CinematicText> texts;

	public GameObject textWindow;

	public Cinematic(){
		numberStep = 0;
		events = new List<CinematicEvent> ();
		texts = new Dictionary<int, CinematicText> ();
	}
	
	public void processStep(){

		foreach (CinematicEvent e in events) {

			if (e.getBeginStep () == numberStep) {
				e.startEvent ();
			} else if (e.getBeginStep () > numberStep && e.getEndStep () < numberStep) {
				
			} else {
				events.Remove (e);
				e.endEvent ();
			}

		}

		bool textFound = false;

		foreach (KeyValuePair<int, CinematicText> entry in texts) {

			if (entry.Key == numberStep) {
				textWindow.SetActive (true);
				setTextWindow (entry.Value.author, entry.Value.text);
				textFound = true;
				break;
			}

		}

		if (!textFound) {
			textWindow.SetActive (false);
		}

	}

	public void progressStep(){

		numberStep++;
		processStep ();

	}

	void setTextWindow(string author, string text){
		
		GameObject authorTextUI = textWindow.transform.FindChild ("Author").gameObject;
		GameObject textUI = textWindow.transform.FindChild ("Text").gameObject;

		authorTextUI.GetComponent<Text> ().text = author;
		textUI.GetComponent<Text> ().text = text;
	}

	public void addTextInList(int step, CinematicText t){
		texts.Add (step, t);
	}
}

public class CinematicText{
	public string author;
	public string text;

	public CinematicText(string author, string text){
		this.author = author;
		this.text = text;
	}
}

public class CinematicBegin : Cinematic{

	public CinematicBegin() : base(){

		CinematicText ct1 = new CinematicText ("Lesten", "Il y a bien longtemps notre monde était prospère. ");
		CinematicText ct2 = new CinematicText ("Lesten", "Malheureusement, un jour, les ténèbres apparurent au coeur de notre capitale. Leurs créatures tuèrent tout ceux qu'ils purent et s'emparèrent de la capitale.");
		CinematicText ct3 = new CinematicText ("Lesten", "Aujourd'hui, les survivants m'ont choisi pour me diriger vers la capitale afin d'essayer à mon tour de vaincre les ténèbres. Je ne sais pas si j'y arriverais mais je dois essayer.");
		CinematicText ct4 = new CinematicText ("Lesten", "C'est ainsi que j'arrive dans la Forêt d'Akeren, une forêt peuplé de faibles créatures");

		addTextInList (0, ct1);
		addTextInList (1, ct2);
		addTextInList (2, ct3);

	}

}