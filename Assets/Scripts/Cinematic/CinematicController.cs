﻿using UnityEngine;
using System.Collections;

public class CinematicController : MonoBehaviour {

	Cinematic cinematic;

	public GameObject textWindow;

	void Start(){
		cinematic = new CinematicBegin ();
		cinematic.textWindow = textWindow;

		cinematic.processStep ();
	}

	void Update(){

		if (Input.GetKeyDown ("space") || Input.GetMouseButtonDown (0)) {
			cinematic.progressStep ();
		}

	}
}
