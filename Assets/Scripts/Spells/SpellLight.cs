﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpellLight : MonoBehaviour {

	public float speed;
	public float distanceKill;

	private bool isReceive;
	private bool isAnimationFinished;

	void Start(){
		isReceive = false;
	}

	void FixedUpdate(){

		if (!isAnimationFinished  && !isReceive) {
			
			Rigidbody2D rigibody = GetComponent<Rigidbody2D> ();
			Vector2 movement = new Vector2 (0, 1);

			rigibody.velocity = movement * speed;

			if (rigibody.position.y > distanceKill) {

				GetComponent<SpriteRenderer> ().color = new Color (0.0f, 0.0f, 0.0f, 0.0f);

				StartCoroutine (flashLight ());
				rigibody.velocity = new Vector2 (0.0f, 0.0f);

				isReceive = true;
			}

		} else if(isAnimationFinished) {

			GameObject.Find ("GameController").GetComponent<GameController> ().isAttackFinished = true;

			Destroy (gameObject);
		}

	}

	IEnumerator flashLight(){

		GameObject flashLight = GameObject.Find ("GameController").GetComponent<GameController> ().flashLight;

		Color c = flashLight.GetComponent<RawImage> ().color;
		c.a = 0.0f;

		flashLight.GetComponent<RawImage> ().color = c;
		flashLight.SetActive (true);

		for (float i = 0.1f; i <= 1.5f; i += 0.1f) {
			c.a = i;
			flashLight.GetComponent<RawImage> ().color = c;

			yield return new WaitForSeconds (0.1f);
		}

		yield return new WaitForSeconds (1f);

		for (float i = 1.0f; i > 0.0f; i -= 0.1f) {
			c.a = i;
			flashLight.GetComponent<RawImage> ().color = c;

			yield return new WaitForSeconds (0.1f);
		}

		flashLight.SetActive (false);

		isAnimationFinished = true;

		yield return null;

	}

}
