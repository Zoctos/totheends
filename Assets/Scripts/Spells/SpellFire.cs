﻿using UnityEngine;
using System.Collections;

public class SpellFire : MonoBehaviour {

	public float speed;
	public float distanceKill;

	void FixedUpdate () {
	
		Rigidbody2D rigibody = GetComponent<Rigidbody2D> ();
		Vector2 movement = new Vector2(0, 1);

		rigibody.velocity = movement * speed;

		if (rigibody.position.y > distanceKill) {
			GameObject.Find ("GameController").GetComponent<GameController> ().isAttackFinished = true;
			Destroy (gameObject);
		}

	}
}
