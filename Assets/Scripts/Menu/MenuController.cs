﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {

		SaveLoadGame.Load ();

		if (SaveLoadGame.playerSave == null) {
			Debug.Log ("premier lancement");
		}

		SaveLoadGame.playerSave.mana = 0;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void startGame(){

		SaveLoadGame.Save ();

		SceneManager.LoadScene ("selectWorld");
	}

	public void startUpgrade(){

		SaveLoadGame.Save ();

		SceneManager.LoadScene ("upgradeMenu");

	}

	public void startCredits(){

		SceneManager.LoadScene ("credits");

	}

	public void quitGame(){
		Application.Quit ();
	}
}
