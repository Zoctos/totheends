﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuSelectionWorldController : MonoBehaviour {

	public GameObject world1;
	public GameObject world2;
	public GameObject world3;
	public GameObject world4;
	public GameObject world5;
	public GameObject world6;
	public GameObject world7;
	public GameObject world8;
	public GameObject world9;
	public GameObject world10;

	public GameObject leftButton;
	public GameObject rightButton;

	private int currentWorld = 1;
	private GameObject currentGameObject;

	void Start(){

		SaveLoadGame.Load ();

		currentGameObject = world1;
		leftButton.SetActive (false);

		if (!canAccessNextWorld()) {
			rightButton.SetActive (false);
		}
	}

	public GameObject getWorldWindow(int num){

		switch (num) {
		case 1:
			return world1;
			break;
		case 2:
			return world2;
			break;
		case 3:
			return world3;
			break;
		case 4:
			return world4;
			break;
		case 5:
			return world5;
			break;
		case 6:
			return world6;
			break;
		case 7:
			return world7;
			break;
		case 8:
			return world8;
			break;
		case 9:
			return world9;
			break;
		case 10:
			return world10;
			break;

		}

		return null;
	}

	public bool canAccessNextWorld(){

		int max = SaveLoadGame.playerSave.maxLevelReach;

		if (max > currentWorld * 10) {
			return true;
		} else {
			return false;
		}
	}

	public void changeWorldWindow(){

		if (currentWorld == 1) {
			leftButton.SetActive (false);
		} else if (!leftButton.activeSelf) {
			leftButton.SetActive (true);
		}

		if (currentWorld == 10 || !canAccessNextWorld()) {
			rightButton.SetActive (false);
		} else if (!rightButton.activeSelf) {
			rightButton.SetActive (true);
		}

		currentGameObject.SetActive (false);

		currentGameObject = getWorldWindow (currentWorld);
		if (currentGameObject != null) {
			currentGameObject.SetActive (true);
		}

	}

	public void onMoveLeft(){
		currentWorld--;
		changeWorldWindow ();
	}

	public void onMoveRight(){
		currentWorld++;
		changeWorldWindow ();
	}

	public void enterWorld(int idZone){

		SaveLoadGame.idZone = idZone;

		SceneManager.LoadScene ("overworld");

	}
}
