﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuUpgradeController : MonoBehaviour {

	public GameObject panelLife;
	public GameObject panelMana;
	public GameObject panelAttack;
	public GameObject panelSpells;
	public GameObject panelSpecial;

	public GameObject buttonLife;
	public GameObject buttonLifeRegen;
	public GameObject buttonMana;
	public GameObject buttonManaRegen;
	public GameObject buttonDamage;
	public GameObject buttonCritic;
	public GameObject buttonFire;
	public GameObject buttonIce;
	public GameObject buttonThunder;
	public GameObject buttonLight;
	public GameObject buttonGold;

	public GameObject lifeStats;
	public GameObject manaStats;
	public GameObject attackStats;
	public GameObject spellStats;
	public GameObject specialStats;

	public GameObject player;

	private PlayerInfo playerInfo;

	private GameObject lastPanel;
	private GameObject lastStats;

	// Use this for initialization
	void Start () {
	
		lastPanel = panelLife;
		lastStats = lifeStats;

		SaveLoadGame.Load ();

		playerInfo = SaveLoadGame.playerSave;

		if (playerInfo.getLevelLife () >= 100)
			buttonLife.SetActive (false);
		else
			setCostButton(buttonLife, (playerInfo.getLevelLife ()+1) * 50);

		if (playerInfo.getLevelLifeRegen () >= 50)
			buttonLifeRegen.SetActive (false);
		else
			setCostButton (buttonLifeRegen, (playerInfo.getLevelLifeRegen () + 1) * 75);

		if (playerInfo.getLevelMana () >= 100)
			buttonMana.SetActive (false);
		else
			setCostButton (buttonMana, (playerInfo.getLevelMana () + 1) * 60);

		if (playerInfo.getLevelManaRegen () >= 50)
			buttonManaRegen.SetActive (false);
		else
			setCostButton (buttonManaRegen, (playerInfo.getLevelManaRegen () + 1) * 85);

		if (playerInfo.getLevelAttack () >= 100)
			buttonDamage.SetActive (false);
		else
			setCostButton (buttonDamage, (playerInfo.getLevelAttack () + 1) * 50);

		if (playerInfo.getLevelCritic () >= 100)
			buttonCritic.SetActive (false);
		else
			setCostButton (buttonCritic, (playerInfo.getLevelCritic () + 1) * 1000);

		if (playerInfo.getLevelFire () >= 100)
			buttonFire.SetActive (false);
		else
			setCostButton (buttonFire, (playerInfo.getLevelFire () + 1) * 60);

		if (playerInfo.getLevelIce () >= 100)
			buttonIce.SetActive (false);
		else
			setCostButton (buttonIce, (playerInfo.getLevelIce () + 1) * 60);

		if (playerInfo.getLevelThunder () >= 100)
			buttonThunder.SetActive (false);
		else
			setCostButton (buttonThunder, (playerInfo.getLevelThunder () + 1) * 100);

		if (playerInfo.getLevelLight () >= 100)
			buttonLight.SetActive (false);
		else
			setCostButton (buttonLight, (playerInfo.getLevelLight () + 1) * 100);

		if (playerInfo.getLevelGold () >= 10)
			buttonGold.SetActive (false);
		else
			setCostButton (buttonGold, (playerInfo.getLevelGold () + 1) * 2000);

		GameObject.Find ("PlayerMoney").GetComponent<Text> ().text = "" + playerInfo.money;

		setLineStat (lifeStats.transform.FindChild ("LifeLine").gameObject, "" + (playerInfo.getLevelLife () * 10 + 20), "" + ((playerInfo.getLevelLife () + 1) * 10 + 20));
		setLineStat (lifeStats.transform.FindChild ("RegenLifeLine").gameObject, "" + playerInfo.getLevelLifeRegen () * 5, "" + (playerInfo.getLevelLifeRegen () + 1) * 5);

		setLineStat (manaStats.transform.FindChild ("ManaLine").gameObject, "" + (playerInfo.getLevelMana () * 10 + 20), "" + ((playerInfo.getLevelMana () + 1) * 10 + 20));
		setLineStat (manaStats.transform.FindChild ("RegenManaLine").gameObject, "" + playerInfo.getLevelManaRegen () * 5, "" + (playerInfo.getLevelManaRegen () + 1) * 5);

		setLineStat (attackStats.transform.FindChild ("DamageLine").gameObject, "" + (playerInfo.getLevelAttack() + 1), "" + (playerInfo.getLevelAttack () + 2));
		setLineStat (attackStats.transform.FindChild ("CriticalLine").gameObject, "" + playerInfo.getLevelCritic() * 0.01f + "%", "" + (playerInfo.getLevelCritic () + 1) * 0.01f + "%" );

		setLineStat (spellStats.transform.FindChild ("FireLine").gameObject, "" + (playerInfo.getLevelFire()+1) * 2, "" + (playerInfo.getLevelFire () + 2) * 2);
		setLineStat (spellStats.transform.FindChild ("IceLine").gameObject, "" + (playerInfo.getLevelIce()+1) * 2, "" + (playerInfo.getLevelIce () + 2) * 2);
		setLineStat (spellStats.transform.FindChild ("ThunderLine").gameObject, "" + (playerInfo.getLevelThunder()+1) * 4, "" + (playerInfo.getLevelThunder () + 2) * 4);
		setLineStat (spellStats.transform.FindChild ("LightLine").gameObject, "" + (playerInfo.getLevelLight()+1) * 2, "" + (playerInfo.getLevelLight () + 2) * 2);

		setLineStat (specialStats.transform.FindChild ("GoldLine").gameObject, "x" + (playerInfo.getLevelGold() + 1), "x" + (playerInfo.getLevelGold () + 2));
	}

	public void setCostButton(GameObject button, int cost){

		button.transform.FindChild ("Cost").gameObject.GetComponent<Text> ().text = "" + cost;

	}

	public void setValueUpgradeButton(GameObject button, int value){

		button.transform.FindChild ("ValueUpgrade").gameObject.GetComponent<Text> ().text = "+" + value;

	}

	public void setLineStat(GameObject line, string current, string next){

		line.transform.FindChild ("CurrentLevelStat").gameObject.GetComponent<Text> ().text = current;
		line.transform.FindChild ("NextLevelStat").gameObject.GetComponent<Text> ().text = next;

	}

	public void disableLastPanel(){

		if (lastPanel != null) {
			lastPanel.SetActive (false);
			lastPanel = null;
		}

		if (lastStats != null) {
			lastStats.SetActive (false);
			lastStats = null;
		}

	}

	public void changeToLifePanel(){

		disableLastPanel ();
		panelLife.SetActive (true);
		lifeStats.SetActive (true);
		lastPanel = panelLife;
		lastStats = lifeStats;

	}

	public void changeToManaPanel(){

		disableLastPanel ();
		panelMana.SetActive (true);
		manaStats.SetActive (true);
		lastPanel = panelMana;
		lastStats = manaStats;

	}

	public void changeToAttackPanel(){

		disableLastPanel ();
		panelAttack.SetActive (true);
		attackStats.SetActive (true);
		lastPanel = panelAttack;
		lastStats = attackStats;

	}

	public void changeToSpellsPanel(){

		disableLastPanel ();
		panelSpells.SetActive (true);
		spellStats.SetActive (true);
		lastPanel = panelSpells;
		lastStats = spellStats;

	}

	public void changeToSpecialPanel(){

		disableLastPanel ();
		panelSpecial.SetActive (true);
		specialStats.SetActive (true);
		lastPanel = panelSpecial;
		lastStats = specialStats;
	}

	public bool playerHaveMoneyToLevelUp(int cost){

		if (playerInfo.money < cost) {
			return false;
		} else {
			return true;
		}

	}

	public void levelUp(string name){

		int cost = 0;
		bool isTransaction = false;

		if (name == "life") {

			cost = (playerInfo.getLevelLife()+1) * 50;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {
				playerInfo.setLevelLife (playerInfo.getLevelLife () + 1);

				if (playerInfo.getLevelLife () >= 100)
					buttonLife.SetActive (false);
				else
					setCostButton(buttonLife, (playerInfo.getLevelLife ()+1) * 50);

				setLineStat (lifeStats.transform.FindChild ("LifeLine").gameObject, "" + (playerInfo.getLevelLife () * 10 + 20), "" + ((playerInfo.getLevelLife () + 1) * 10 + 20));
			}

		} else if (name == "lifeRegen") {

			cost = (playerInfo.getLevelLifeRegen()+1) * 75;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {
				playerInfo.setLevelLifeRegen (playerInfo.getLevelLifeRegen () + 1);

				if (playerInfo.getLevelLifeRegen () >= 50)
					buttonLifeRegen.SetActive (false);
				else
					setCostButton (buttonLifeRegen, (playerInfo.getLevelLifeRegen () + 1) * 75);

				setLineStat (lifeStats.transform.FindChild ("RegenLifeLine").gameObject, "" + playerInfo.getLevelLifeRegen () * 5, "" + (playerInfo.getLevelLifeRegen () + 1) * 5);

			}

		} else if (name == "mana") {
			
			cost = (playerInfo.getLevelMana () + 1) * 60;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelMana (playerInfo.getLevelMana () + 1);

				if (playerInfo.getLevelMana () >= 100)
					buttonMana.SetActive (false);
				else
					setCostButton (buttonMana, (playerInfo.getLevelMana () + 1) * 60);

				setLineStat (manaStats.transform.FindChild ("ManaLine").gameObject, "" + (playerInfo.getLevelMana () * 10 + 20), "" + ((playerInfo.getLevelMana () + 1) * 10 + 20));

			}

		} else if (name == "manaRegen") {

			cost = (playerInfo.getLevelManaRegen () + 1) * 85;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelManaRegen (playerInfo.getLevelManaRegen () + 1);

				if (playerInfo.getLevelManaRegen () >= 50)
					buttonManaRegen.SetActive (false);
				else
					setCostButton (buttonManaRegen, (playerInfo.getLevelManaRegen () + 1) * 85);

				setLineStat (manaStats.transform.FindChild ("RegenManaLine").gameObject, "" + playerInfo.getLevelManaRegen () * 5, "" + (playerInfo.getLevelManaRegen () + 1) * 5);

			}

		} else if (name == "attack") {

			cost = (playerInfo.getLevelAttack () + 1) * 50;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelAttack (playerInfo.getLevelAttack () + 1);

				if (playerInfo.getLevelAttack () >= 100)
					buttonDamage.SetActive (false);
				else
					setCostButton (buttonDamage, (playerInfo.getLevelAttack () + 1) * 50);

				setLineStat (attackStats.transform.FindChild ("DamageLine").gameObject, "" + (playerInfo.getLevelAttack() + 1), "" + (playerInfo.getLevelAttack () + 2));

			}

		} else if (name == "critic") {

			cost = (playerInfo.getLevelCritic () + 1) * 1000;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelCritic (playerInfo.getLevelCritic () + 1);

				if (playerInfo.getLevelCritic () >= 100)
					buttonCritic.SetActive (false);
				else
					setCostButton (buttonCritic, (playerInfo.getLevelCritic () + 1) * 1000);

				setLineStat (attackStats.transform.FindChild ("CriticalLine").gameObject, "" + playerInfo.getLevelCritic() * 0.01f + "%", "" + (playerInfo.getLevelCritic () + 1) * 0.01f + "%" );

			}

		} else if (name == "fire") {

			cost = (playerInfo.getLevelFire () + 1) * 60;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelFire (playerInfo.getLevelFire () + 1);

				if (playerInfo.getLevelFire () >= 100)
					buttonFire.SetActive (false);
				else
					setCostButton (buttonFire, (playerInfo.getLevelFire () + 1) * 60);

				setLineStat (spellStats.transform.FindChild ("FireLine").gameObject, "" + (playerInfo.getLevelFire()+1) * 2, "" + (playerInfo.getLevelFire () + 2) * 2);

			}

		} else if (name == "ice") {

			cost = (playerInfo.getLevelIce () + 1) * 60;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelIce (playerInfo.getLevelIce () + 1);

				if (playerInfo.getLevelIce () >= 100)
					buttonIce.SetActive (false);
				else
					setCostButton (buttonIce, (playerInfo.getLevelIce () + 1) * 60);

				setLineStat (spellStats.transform.FindChild ("IceLine").gameObject, "" + (playerInfo.getLevelIce()+1) * 2, "" + (playerInfo.getLevelIce () + 2) * 2);
				
			}

		} else if (name == "thunder") {

			cost = (playerInfo.getLevelThunder () + 1) * 100;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelThunder (playerInfo.getLevelThunder () + 1);

				if (playerInfo.getLevelThunder () >= 100)
					buttonThunder.SetActive (false);
				else
					setCostButton (buttonThunder, (playerInfo.getLevelThunder () + 1) * 100);

				setLineStat (spellStats.transform.FindChild ("ThunderLine").gameObject, "" + (playerInfo.getLevelThunder()+1) * 4, "" + (playerInfo.getLevelThunder () + 2) * 4);

			}

		} else if (name == "light") {

			cost = (playerInfo.getLevelLight () + 1) * 100;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelLight (playerInfo.getLevelLight () + 1);

				if (playerInfo.getLevelLight () >= 100)
					buttonLight.SetActive (false);
				else
					setCostButton (buttonLight, (playerInfo.getLevelLight () + 1) * 100);

				setLineStat (spellStats.transform.FindChild ("LightLine").gameObject, "" + (playerInfo.getLevelLight()+1) * 2, "" + (playerInfo.getLevelLight () + 2) * 2);

			}

		} else if (name == "money") {

			cost = (playerInfo.getLevelGold () + 1) * 2000;
			isTransaction = playerHaveMoneyToLevelUp (cost);

			if (isTransaction) {

				playerInfo.setLevelGold (playerInfo.getLevelGold () + 1);

				if (playerInfo.getLevelGold () >= 10)
					buttonGold.SetActive (false);
				else
					setCostButton (buttonGold, (playerInfo.getLevelGold () + 1) * 2000);

				setLineStat (specialStats.transform.FindChild ("GoldLine").gameObject, "x" + (playerInfo.getLevelGold() + 1), "x" + (playerInfo.getLevelGold () + 2));

			}

		}

		if (isTransaction) {
			playerInfo.addMoney (-cost);
			GameObject.Find ("PlayerMoney").GetComponent<Text> ().text = "" + playerInfo.money;
		}

	}

	public void backToMainMenu(){

		SaveLoadGame.Save ();
		SceneManager.LoadScene ("menu");

	}
}
