﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class PlayerInfo{

	public int life;
	public int lifeMax;
	public int mana;
	public int manaMax;

	public int money;

	public int levelLife;
	public int levelLifeRegen;
	public int levelMana;
	public int levelManaRegen;
	public int levelAttack;
	public int levelCritic;
	public int levelFire;
	public int levelIce;
	public int levelThunder;
	public int levelLight;
	public int levelGold;

	public int maxLevelReach;

	public PlayerInfo(){

		life = 20;
		lifeMax = 20;
		mana = 20;
		manaMax = 20;

		money = 0;

		levelLife = 0;
		levelLifeRegen = 0;
		levelMana = 0;
		levelManaRegen = 0;
		levelAttack = 0;
		levelCritic = 0;
		levelFire = 0;
		levelIce = 0;
		levelThunder = 0;
		levelLight = 0;
		levelGold = 0;

		maxLevelReach = 0;

	}

	public void heal(int heal){

		life = life + heal;

		if (life > lifeMax)
			life = lifeMax;

	}

	public void healMana(int heal){

		mana = mana + heal;

		if (mana > manaMax)
			mana = manaMax;

	}

	public void TakeDamage(int damage){

		if (life - damage <= 0) {

			life = 0;

		} else {
			life -= damage;
		}

	}

	public void useMana(int manaUsed){

		if (mana - manaUsed < 0) {
			mana = 0;
		} else {
			mana -= manaUsed;
		}

	}

	public void addMoney(int money){
		if (money > 0)
			this.money += money * (levelGold + 1);
		else
			this.money += money;
	}

	public void applySkillsEffect(){

		if (levelLife > 0) {
			lifeMax = (20 + 10 * levelLife);
		}

		if (levelMana > 0) {
			manaMax = (20 + 10 * levelMana);
		}

	}

	public void healAll(){

		this.life = lifeMax;
		this.mana = manaMax;

	}

	public bool getIsDead(){
		if (life <= 0)
			return true;
		else
			return false;
	}

	public int getLevelLife(){
		return this.levelLife;
	}
	public int getLevelLifeRegen(){
		return this.levelLifeRegen;
	}
	public int getLevelMana(){
		return this.levelMana;
	}
	public int getLevelManaRegen(){
		return this.levelManaRegen;
	}
	public int getLevelAttack(){
		return this.levelAttack;
	}
	public int getLevelCritic(){
		return this.levelCritic;
	}
	public int getLevelFire(){
		return this.levelFire;
	}
	public int getLevelIce(){
		return this.levelIce;
	}
	public int getLevelThunder(){
		return this.levelThunder;
	}
	public int getLevelLight(){
		return this.levelLight;
	}
	public int getLevelGold(){
		return this.levelGold;
	}

	public void setLevelLife(int levelLife){
		this.levelLife = levelLife;
	}
	public void setLevelLifeRegen(int levelLifeRegen){
		this.levelLifeRegen = levelLifeRegen;
	}
	public void setLevelMana(int levelMana){
		this.levelMana = levelMana;
	}
	public void setLevelManaRegen(int levelManaRegen){
		this.levelManaRegen = levelManaRegen;
	}
	public void setLevelAttack(int levelAttack){
		this.levelAttack = levelAttack;
	}
	public void setLevelCritic(int levelCritic){
		this.levelCritic = levelCritic;
	}
	public void setLevelFire(int levelFire){
		this.levelFire = levelFire;
	}
	public void setLevelIce(int levelIce){
		this.levelIce = levelIce;
	}
	public void setLevelThunder(int levelThunder){
		this.levelThunder = levelThunder;
	}
	public void setLevelLight(int levelLight){
		this.levelLight = levelLight;
	}
	public void setLevelGold(int levelGold){
		this.levelGold = levelGold;
	}
	public void setMaxLevelReach(int level){
		if (maxLevelReach < level)
			maxLevelReach = level;
	}
}
