﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoadGame {

	public static PlayerInfo playerSave;
	public static int idZone = -1;

	public static void Save(){

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playersave.gd");
		bf.Serialize (file, playerSave);
		file.Close ();

	}

	public static void Load(){

		//Debug.Log (Application.persistentDataPath);

		if (File.Exists (Application.persistentDataPath + "/playersave.gd")) {

			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playersave.gd", FileMode.Open);
			playerSave = (PlayerInfo)bf.Deserialize (file);
			file.Close ();

		} else {
			playerSave = new PlayerInfo ();
			Save ();
		}

		playerSave.applySkillsEffect ();

	}

}
