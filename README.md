### What is this project ? ###

This project is a game named "To The Ends". It's a little game made by one person with using a lot of ressources that everybody can use for his project. The game is made with the engine Unity.

In this game, you control a character who must save the world from darkness. For the moment the game is only a sequel of fighting. In a fight you have 5 actions : use your sword or use 4 spells : Fire, Ice, Thunder and Light. 

### Credits ###

+ Development / Idea / Script : Pierre Thuillier-Le Gac
+ Sprites / Graphics :
	- Pixel Art Icons : Henrique Lazarini
	- LPC Base Assets :
		- Lanea Zimmerman
		- Stephen Challener
		- Charles Sanchez
		- Manuel Riecke
		- Daniel Armstrong
+ Tools Used :
	- Tiled
	- Tield2Unity
	- Unity

### Last words ###

Thanks for playing